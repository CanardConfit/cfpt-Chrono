﻿namespace Chrono
{
    partial class FrmChrono
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gbxTour = new System.Windows.Forms.GroupBox();
            this.lblTour = new System.Windows.Forms.Label();
            this.lbxTours = new System.Windows.Forms.ListBox();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnTour = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.lblHeader = new System.Windows.Forms.Label();
            this.gbxTotal = new System.Windows.Forms.GroupBox();
            this.lblTotal = new System.Windows.Forms.Label();
            this.tmrChrono = new System.Windows.Forms.Timer(this.components);
            this.gbxTour.SuspendLayout();
            this.gbxTotal.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbxTour
            // 
            this.gbxTour.Controls.Add(this.lblTour);
            this.gbxTour.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.gbxTour.Location = new System.Drawing.Point(9, 118);
            this.gbxTour.Name = "gbxTour";
            this.gbxTour.Size = new System.Drawing.Size(402, 100);
            this.gbxTour.TabIndex = 0;
            this.gbxTour.TabStop = false;
            this.gbxTour.Text = "Tour";
            // 
            // lblTour
            // 
            this.lblTour.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblTour.Location = new System.Drawing.Point(3, 18);
            this.lblTour.Name = "lblTour";
            this.lblTour.Size = new System.Drawing.Size(393, 79);
            this.lblTour.TabIndex = 0;
            this.lblTour.Text = "0:00:00.0";
            this.lblTour.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbxTours
            // 
            this.lbxTours.FormattingEnabled = true;
            this.lbxTours.ItemHeight = 16;
            this.lbxTours.Location = new System.Drawing.Point(9, 270);
            this.lbxTours.Name = "lbxTours";
            this.lbxTours.Size = new System.Drawing.Size(402, 212);
            this.lbxTours.TabIndex = 1;
            // 
            // btnReset
            // 
            this.btnReset.Enabled = false;
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.btnReset.Location = new System.Drawing.Point(9, 490);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(130, 45);
            this.btnReset.TabIndex = 2;
            this.btnReset.Text = "R.A.Z.";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btns_Click);
            // 
            // btnTour
            // 
            this.btnTour.Enabled = false;
            this.btnTour.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.btnTour.Location = new System.Drawing.Point(145, 490);
            this.btnTour.Name = "btnTour";
            this.btnTour.Size = new System.Drawing.Size(130, 45);
            this.btnTour.TabIndex = 3;
            this.btnTour.Text = "Tour";
            this.btnTour.UseVisualStyleBackColor = true;
            this.btnTour.Click += new System.EventHandler(this.btns_Click);
            // 
            // btnStart
            // 
            this.btnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.btnStart.Location = new System.Drawing.Point(281, 490);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(130, 45);
            this.btnStart.TabIndex = 4;
            this.btnStart.Text = "Démarrer";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btns_Click);
            // 
            // lblHeader
            // 
            this.lblHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblHeader.Location = new System.Drawing.Point(9, 244);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(402, 23);
            this.lblHeader.TabIndex = 1;
            this.lblHeader.Text = "Tour No                 Total                             Ce tour";
            // 
            // gbxTotal
            // 
            this.gbxTotal.Controls.Add(this.lblTotal);
            this.gbxTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.gbxTotal.Location = new System.Drawing.Point(9, 12);
            this.gbxTotal.Name = "gbxTotal";
            this.gbxTotal.Size = new System.Drawing.Size(402, 100);
            this.gbxTotal.TabIndex = 1;
            this.gbxTotal.TabStop = false;
            this.gbxTotal.Text = "Total";
            // 
            // lblTotal
            // 
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold);
            this.lblTotal.Location = new System.Drawing.Point(3, 18);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(393, 79);
            this.lblTotal.TabIndex = 0;
            this.lblTotal.Text = "0:00:00.0";
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tmrChrono
            // 
            this.tmrChrono.Interval = 1;
            this.tmrChrono.Tick += new System.EventHandler(this.tmrChrono_Tick);
            // 
            // FrmChrono
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(423, 544);
            this.Controls.Add(this.gbxTotal);
            this.Controls.Add(this.lblHeader);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.btnTour);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.lbxTours);
            this.Controls.Add(this.gbxTour);
            this.Name = "FrmChrono";
            this.Text = "Form1";
            this.gbxTour.ResumeLayout(false);
            this.gbxTotal.ResumeLayout(false);
            this.ResumeLayout(false);
        }

        private System.Windows.Forms.Timer tmrChrono;

        private System.Windows.Forms.GroupBox gbxTotal;
        private System.Windows.Forms.Label lblTotal;

        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnTour;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.GroupBox gbxTour;
        private System.Windows.Forms.Label lblTour;
        private System.Windows.Forms.Label lblHeader;
        private System.Windows.Forms.ListBox lbxTours;

        #endregion
    }
}
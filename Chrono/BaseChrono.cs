﻿using System;

namespace Chrono
{
    public class BaseChrono : ICloneable
    {
        private DateTime _unChrono;
        private BaseChrono _totalChrono;

        public BaseChrono()
        {
            _unChrono = DateTime.Now;
        }

        public BaseChrono(int heure, int minute, int seconde) : this()
        {
            _unChrono = new DateTime(_unChrono.Year, _unChrono.Month, _unChrono.Day, heure, minute, seconde);
        }

        public DateTime Date => DateTime.Today + (DateTime.Now - _unChrono);

        public BaseChrono TotalChrono
        {
            get => _totalChrono;
            set => _totalChrono = value;
        }

        public void AddTick(double millisecondes)
        {
            _unChrono = _unChrono.AddMilliseconds(millisecondes);
        }

        public object Clone()
        {
            return new BaseChrono(_unChrono.Hour, _unChrono.Minute, _unChrono.Second);
        }
    }
}
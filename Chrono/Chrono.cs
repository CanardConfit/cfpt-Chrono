﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Chrono
{
    public class Chrono
    {
        private BaseChrono _total;
        private List<BaseChrono> _tours;

        public BaseChrono Tour => _tours.Last();
        public BaseChrono Total => _total;
        public List<BaseChrono> Tours => _tours;
        public int TourNo => _tours.Count;
        public EventHandler NewTour;

        public Chrono() : this(DateTime.Now.Hour,DateTime.Now.Minute,DateTime.Now.Second)
        {
        }

        public Chrono(int heure, int minute, int seconde)
        {
            _total = new BaseChrono(heure, minute, seconde);
            _tours = new List<BaseChrono> {new(heure, minute, seconde)};
        }

        public void NouveauTour()
        {
            Tour.TotalChrono = (BaseChrono) _total.Clone();
            _tours.Insert(_tours.Count-1,new BaseChrono());
            NewTour?.Invoke(this, EventArgs.Empty);
        }
    }
}
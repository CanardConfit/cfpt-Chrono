﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace Chrono
{
    public partial class FrmChrono : Form
    {
        private Chrono _chrono;
        
        public FrmChrono()
        {
            InitializeComponent();
        }
        
        

        private void btns_Click(object sender, EventArgs e)
        {
            if (sender is not Button btn) return;
            switch (btn.Name)
            {
                case "btnReset":
                    tmrChrono.Enabled = false;
                    btnReset.Enabled = btnTour.Enabled = false;
                    btnStart.Enabled = true;
                    break;
                case "btnStart":
                    _chrono = new Chrono();
                    _chrono.NewTour += Chrono_NewTour;
                    btnReset.Enabled = btnTour.Enabled = true;
                    btnStart.Enabled = false;
                    tmrChrono.Enabled = true;
                    break;
                case "btnTour":
                    _chrono.NouveauTour();
                    break;
            }
        }

        private void tmrChrono_Tick(object sender, EventArgs e)
        {
            lblTotal.Text = $"{_chrono.Total.Date:hh:mm:ss.fff}";
            lblTour.Text = $"{_chrono.Tour.Date:hh:mm:ss.fff}";
        }
        private void Chrono_NewTour(object sender, EventArgs e)
        {
            lbxTours.DataSource = _chrono.Tours.Where(chrono => chrono.TotalChrono != null).Select(chrono => $"Tour {_chrono.Tours.IndexOf(chrono)}                {chrono.TotalChrono.Date:hh:mm:ss.fff}                {chrono.Date:hh:mm:ss.fff}").ToList();
        }
    }
}